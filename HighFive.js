/**
 * @param {number[][]} items
 * @return {number[][]}
 */
var highFive = function(items) {
    
    let id_amount_sum_scores = [];
    
    id_amount_sum_scores.push([items[0][0], 1, items[0][1], [items[0][1]]]);
    console.log(id_amount_sum_scores);
    
    for(i=1; i<items.length; i++) {
        // console.log('************************');
        // console.log('new item: ', items[i]);

        let id_exits = -1;
        let j;
        for(id=0; id<id_amount_sum_scores.length; id++) {
            if(items[i][0] == id_amount_sum_scores[id][0]) {
                id_exits = 1;
                j = id;
            }
        }
        
        // is a new id
        if(id_exits == -1) {
            // console.log('new id, insert');
            
            // insert the whole array
            id_amount_sum_scores.push([items[i][0], 1, items[i][1], [items[i][1]]]);
            // console.log('id_amount_sum_scores length: ', id_amount_sum_scores.length);
        }        
        // not a new id
        else {
            // the id now has less than 5 records
            if(id_amount_sum_scores[j][1] < 5) {
                // console.log('not new but first 5');
                // records ++
                id_amount_sum_scores[j][1] += 1;
                // compute new sum
                id_amount_sum_scores[j][2] += items[i][1];
                // add new score
                id_amount_sum_scores[j][3].push(items[i][1]);
                // update top 5 scores in order
                id_amount_sum_scores[j][3].sort(function(a, b){return b - a});
            }

            // the id already has 5 records
            else {
                // console.log('not new but higher than the lowest');
                // new score is higher than the lowest
                if(items[i][1] > id_amount_sum_scores[j][3][4]) {
                    // compute new sum
                    
                    id_amount_sum_scores[j][2] = (id_amount_sum_scores[j][2] - id_amount_sum_scores[j][3][4]) + items[i][1];
                    // substitue the lowest score with new score
                    id_amount_sum_scores[j][3][4] = items[i][1];
                    // update top 5 scores in order
                    id_amount_sum_scores[j][3].sort(function(a, b){return b - a});
                }
            }
        }
        // console.log(id_amount_sum_scores);
    }
    
    // console.log('------', id_amount_sum_scores, '-----');
    
    // prepare the return array
    let returnArray = [];
    for(i=0; i<id_amount_sum_scores.length; i++) {
        id = id_amount_sum_scores[i][0];
        floor_average = Math.floor(id_amount_sum_scores[i][2]/5);
        returnArray.push([id, floor_average]);
    }
    
    return returnArray;
};