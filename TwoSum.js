/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */



var twoSum = function(nums, target) {
    let returnArray = [];
    
    for (i=0; i<nums.length; i++) {
        let residual = target - nums[i];
        for (j=i+1; j<nums.length; j++) {
            if (nums[j] == residual) {
                returnArray[0] = i;
                returnArray[1] = j;
                return returnArray;
            }
        }
    }
};