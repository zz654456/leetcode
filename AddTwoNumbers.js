/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * @param {ListNode} l1
 * @param {ListNode} l2
 * @return {ListNode}
 */

 
var addTwoNumbers = function(l1, l2) {
    let current = new ListNode(0);
    let returnList = current;
    while(l1 || l2){
        if(!l1) {
            current.next = new ListNode(l2.val);
            l2 = l2.next;
        } else if(!l2) {
            current.next = new ListNode(l1.val);
            l1 = l1.next;
        } else {
            sum = l1.val + l2.val;
            if(sum >= 10){
                current.val = current.val + 1;
            }
            current.next = new ListNode(sum % 10);
            l1 = l1.next;
            l2 = l2.next;
        }
        current = current.next;
    }
    return returnList.next;
}




